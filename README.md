# Laphroaig Theme Documentation

---

**Dependecnies**

* [Premise WP](https://github.com/PremiseWP/Premise-WP)

* [Grunt](https://gruntjs.com/)

### Grunt

Before installing Grunt, ensure that your npm is up to date.

```cli
$ npm update -g npm
```

Install Grunt CLI globally.

```cli
$ npm install -g grunt-cli
```

To run Grunt, `cd` to the theme's directory and run the following command from there; This will install the version of Grunt the project uses and its dependencies.

```cli
$ npm install
```

Now, run Grunt.

```cli
$ grunt
```

### Premise WP

Premise is a Wordpress Plugin but it has no user interface. Premise WP is a framework for Wordpress that allows us to do certain things in the backedn faster, like creating custom post types or meta boxes.

Premise WP also has a small but helpful CSS framework. All Premise WP classes begin with `premise-` or `pwp-`.

---

## Theme Structure

* `front-page.php` - Single page site. No other templates where developed.
* `sections` - This directory holds each section of the single page site.
* `_front-page.scss` - Controls the front page styles. Located at `sass/site/primary/`.
* `js` - files are located at `js/source/`.

_This theme was built using [\_underscore](https://underscores.me/). API documentation can be found [here](docs/index.html)_