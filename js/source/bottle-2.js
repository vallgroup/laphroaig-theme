/**
 * animate the bottle ingredients
 *
 * @return {Object} instance of this object
 */
function pwpsLaphroaigBottle2() {
	$ = jQuery;

	this.makingLaphroaig            = $('.section-making-laphroaig-ingredients'),
	this.makingLaphroaigIngredients = this.makingLaphroaig.find('.section-making-laphroaig-ingredients-i'),
	this.bottle                     = this.makingLaphroaig.find('.section-making-laphroaig-ingredients-bottle'),
	this.count                      = this.makingLaphroaigIngredients.length,
	this._index                     = 0,
	this._interval                  = 0,
	this._offset                    = 255;

	// if ( 'ontouchstart' in window ) {
	// 	this.makingLaphroaigIngredients.addClass('animated').height('auto');
	// }
	// else if ( this.makingLaphroaig.length ) {
	// 	this.bindScroll();
	// }
	this.bindScroll();

	return this;
};

/**
 * bind the scroll animation for the bottle
 *
 * @return {void} does not return anything
 */
pwpsLaphroaigBottle2.prototype.bindScroll = function() {
	var $this = this;
	this.makingLaphroaig.premiseScroll({
		offset: 109,
		onScroll: function() {
			if ( this.scrolled() < ( ($this.bottle.offset().top + $(window).height()) - $this._offset ) ) {
				if( 'up' === this.directionScrolled() ) {
					$this.backInTheBottle(this.pixelsScrolled());
				}
				else if ( $this._index < $this.count ) {
					$this.animateIngredient(this.pixelsScrolled());
				}
				if ( $this._index === $this.count ) {
					return;
				}
			}
			else {
				$this.makingLaphroaigIngredients.addClass('animated').height('auto');
				$this._index = $this.count;
				return;
			}
		}
	})
};

/**
 * animate the ingredient from the bottle
 *
 * @return {void} does not return anything
 */
pwpsLaphroaigBottle2.prototype.animateIngredient = function(px) {
	var elHeight    = $(this.makingLaphroaigIngredients[ this._index ]).height();
	var innerHeight = $(this.makingLaphroaigIngredients[ this._index ]).children().first().height();

	if ( elHeight > 50 ) {
		$(this.makingLaphroaigIngredients[ this._index ]).addClass('animated');
	}
	if ( elHeight < innerHeight ) {
		$(this.makingLaphroaigIngredients[ this._index ]).height(elHeight + px);
	}
	else {
		this._index    = this._index + 1;
	}
};

pwpsLaphroaigBottle2.prototype.backInTheBottle = function(px) {
	var elHeight    = $(this.makingLaphroaigIngredients[ this._index ]).height();
	var innerHeight = $(this.makingLaphroaigIngredients[ this._index ]).children().first().height();

	if ( elHeight > 50 ) {
		$(this.makingLaphroaigIngredients[ this._index ]).removeClass('animated');
	}
	if ( elHeight > 0 ) {
		$(this.makingLaphroaigIngredients[ this._index ]).height(elHeight + px);
	}
	else {
		this._index    = this._index - 1;
	}
};