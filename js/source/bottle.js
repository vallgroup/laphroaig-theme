/**
 * animate the bottle ingredients
 *
 * @return {Object} instance of this object
 */
function pwpsLaphroaigBottle() {
	$ = jQuery;

	this.makingLaphroaig            = $('.section-making-laphroaig-ingredients'),
	this.makingLaphroaigIngredients = this.makingLaphroaig.find('.section-making-laphroaig-ingredients-i'),
	this.count                      = this.makingLaphroaigIngredients.length,
	this._index                     = 0,
	this._interval                  = 0,
	this._offset                    = 255;

	if ( this.makingLaphroaig.length ) {
		this.bindScroll();
	}

	return this;
};

/**
 * bind the scroll animation for the bottle
 *
 * @return {void} does not return anything
 */
pwpsLaphroaigBottle.prototype.bindScroll = function() {
	var $this = this,

	animation = this.makingLaphroaig.premiseScroll({
		offset: (jQuery(window).width() >= 768) ? $this._offset : 100,
		onScroll: function() {
			// if we have animated all ingredients stop
			if ( $this._index === $this.count ) {
				return;
				// this.stopScroll();
			}

			if ( 'down' === this.directionScrolled() ) {
				// if we still have ingredients to load
				// or the user has scrolled past _offset since
				// the last ingredient was animated
				if ( $this._index < $this.count
					&& $this._interval < this.totalScrolled() ) {
					$this.animateIngredient();
				}

			}

			else {
				console.log(this.totalScrolled());
				if ( $this._index > $this.count
					&& $this._interval > this.totalScrolled() ) {
					$this.backInTheBottle();
				}
			}
		}
	});

	// $(window).scroll(function() {
	// 	if (animation && 0 === jQuery('html body').scrollTop() ) {
	// 		jQuery(this.makingLaphroaigIngredients).removeClass('animated');
	// 		this._index    = 0;
	// 		this._interval = 0;
	// 		animation.startScroll();
	// 	}
	// }.bind(this));
	// $('body').animate({
	// 	scrollTop: 3040,
	// }, 400);
};

/**
 * animate the ingredient from the bottle
 *
 * @return {void} does not return anything
 */
pwpsLaphroaigBottle.prototype.animateIngredient = function() {

	this.makingLaphroaigIngredients[ this._index ].className += ' animated';

	this._index    = this._index + 1;
	this._interval = this._interval + this._offset;
};

pwpsLaphroaigBottle.prototype.backInTheBottle = function() {

	$(this.makingLaphroaigIngredients[ this._index ]).removeClass('animated');

	this._index    = this._index - 1;
	this._interval = this._interval - this._offset;
};