(function($){
	$(document).ready(function(){
		var _doc = $(this); // jQuery object for document

		// var Bottle = new pwpsLaphroaigBottle();
		var Bottle = new pwpsLaphroaigBottle2();

		var FlovarWheel = new pwpsLaphroaigFlavorWheel();

		$('.menu-toggle').click(function(){
			$('.site-content, .pwp-simplicity-hero').one('click', function(e){
				e.stopPropagation();
				$('#site-navigation').removeClass('toggled');

			});
		});
		// bind the nav scroll
		$('#primary-menu a').click(function(e){
			e.preventDefault();
			var _href = $(this).attr('href');
			var _rgxp = new RegExp(/^#[^#]+/, 'g');
			var _mtch = _href.match(_rgxp);

			if ( null !== _mtch && $(_href).length ) {
				$('#site-navigation').removeClass('toggled');
				if ( $(_href).offset().top > Bottle.makingLaphroaig.offset().top ) {
					Bottle.makingLaphroaigIngredients.height('auto');
				}
				$('html, body').animate({
					scrollTop: $(_href).offset().top,
				}, 1000);
			}
			return false;
		});

		$(window).scroll( function(e) {
			var _scrollTop = $('html, body').scrollTop();
			if ( 80 < _scrollTop ) {
				$('.site-header').addClass('shrink');
			}
			else {
				$('.site-header').removeClass('shrink');
			}
		});

	});

	$(window).load(function(){
		var _win = $(this); // jQuery object for window

		// more code here
	});
})(jQuery)