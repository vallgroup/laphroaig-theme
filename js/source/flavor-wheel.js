/**
 * Rotate the wheel
 *
 * @return {Object} an instance for the flavor wheel object
 */
function pwpsLaphroaigFlavorWheel() {
	$ = jQuery;

	this.flavorWheelCont     = $('.section-flavors-laphroaig-wheel-cont'),
	this.flavorWheelScroller = this.flavorWheelCont.find('.section-flavors-laphroaig-wheel-scroller'),
	this.svg                 = this.flavorWheelScroller.find('svg'),
	this.rotated             = 0,
	this.body                = $('html, body'),
	this.touchStartY         = 0,
	this.firstCocktail       = null,
	this.allCocktails        = [],
	this.cocktailsGrid       = $('.section-more-cocktails-grid'),
	this.loadingIcon         = '<i class="fa fa-spinner fa-spin"></i>',
	this._food               = $('.section-flavors-laphroaig-food'),
	this._info               = this.flavorWheelCont.find('.section-flavors-laphroaig-wheel-info'),
	this._cocktail           = $('.section-flavors-laphroaig-cocktail')
	this.lightbox            = $('.section-more-cocktails-lightbox');

	//set the initial rotation to 0
	this.svg.css('transform', 'rotate('+this.rotated+'deg)');
	this.svg.find('#flavor-wheel > g#smoke').addClass('hovered');

	this.__oldY = 0;

	this.loadNewFlavor();
	// bind the wheel!
	this.bindEvents();

	return this;
};

/**
 * Move the actual wheel
 *
 * @param  {integer} n number of pixels to move by
 */
pwpsLaphroaigFlavorWheel.prototype.moveTheWheel = function(n) {
	n = n || 0;
	if (0 === n) return;
	var scrolled = (this.rotated + (n));

	// From: matrix(-0.839072, 0.544021, -0.544021, -0.839072, 0, 0)
	// To:   ["-0.839072", " 0.544021", " -0.544021", " -0.839072", " 0", " 0"]
	var matrix = this.svg.css('transform').match(/^matrix\(+([^)]*)+\)$/)[1].split(',');

	var newMatrix = [
		( Math.cos( scrolled ) ),
		( Math.sin( scrolled ) ),
		(-Math.sin( scrolled ) ),
		( Math.cos( scrolled ) ),
		( +matrix[4] + 0 ),
		( +matrix[5] + 0 )
	];

	// rotate the wheel
	this.svg.css('transform', 'matrix('+newMatrix.join(',')+')');

	// save the amount rotated
	this.rotated = scrolled;
};

/**
 * rotate the wheel whenon touch screeen
 *
 * @param  {object} e the event triggered when the finger moves
 */
pwpsLaphroaigFlavorWheel.prototype.touchMove = function(e){
	e           = e           || window.event;
	// save the new Y position of the finger
	var __newY = e.originalEvent.touches[0].clientY - this.touchStartY - this.__oldY;
	// move the wheel
	this.moveTheWheel((((__newY + this.__oldY) * -1) * 0.00025));
	// save the position for the next event
	this.__oldY = __newY;
	e.preventDefault();
};

/**
 * fores when the user places the finger on the wheel to rotate it
 *
 * @param  {object} e the event fired
 */
pwpsLaphroaigFlavorWheel.prototype.touchStart = function(e){
	this.touchStartY = e.touches[0].clientY;
	// Bind move, then touchend right after
	this.svg.on('touchmove', this.touchMove.bind(this));
	this.svg.one('touchend', function(){
		$('.site').removeClass('noscroll');
		this.svg.off('touchmove', this.touchMove);
	}.bind(this));
};

/**
 * this callback fores when the user is on a desktop and scrolls with the sursor on the wheel
 *
 * @param  {object} e the event fired on scroll (moving the wheel)
 */
pwpsLaphroaigFlavorWheel.prototype.onWheel = function(e) {
	e = e || window.event;
	this.moveTheWheel(e.deltaY * 0.025);
};

/**
 * this fires when the mouse moves over the wheel on desktop
 */
pwpsLaphroaigFlavorWheel.prototype.scrollMove = function(e) {
	this.body.addClass('noscroll');
	// make our wheel rotate
	window.onwheel = this.onWheel.bind(this);
	this.svg.one('mouseleave', function() {
		this.body.removeClass('noscroll');
		window.onwheel = null;
	}.bind(this));
};

/**
 * called from our init function after building our object, this function binds all events necessary for the weehl to rotate.
 */
pwpsLaphroaigFlavorWheel.prototype.bindEvents = function() {
	var _this = this;
	// bind the wheel movement for desktop and touch
	if('ontouchstart' in window) {
		this.svg[0].addEventListener('touchstart', this.touchStart.bind(this), false);
	} else {
		this.svg.on('mouseenter', this.scrollMove.bind(this));
	}
	// bind the wheel flavor click
	this.svg.find('#flavor-wheel > g:not(#circle)').on('click', function(e){
		e.preventDefault();

		_this.svg.find('#flavor-wheel > g').removeClass('hovered');
		var $this = jQuery(this);
		// if this is clicked already return.
		if ( $this.is('.hovered') ) {
			return false;
		}

		$this.addClass('hovered');
		_this.loadNewFlavor();
		return false;
	});

	this.bindCocktailLightbox();
};

pwpsLaphroaigFlavorWheel.prototype.loadNewFlavor = function() {
	// get the flavor
	var _hovered = this.svg.find('#flavor-wheel > g.hovered'),
	_flavor = _hovered.attr('id').trim();

	// clear the cocktails
	this.firstCocktail = null;
	this.allCocktails  = [];
	// display the loading icon where
	// we will be outputting data
	this._food.find('p').html( this.loadingIcon );
	this._info.find('h3').html( this.loadingIcon );
	this._info.find('.flavor-content').html( this.loadingIcon );
	this._cocktail.find('h2').html( this.loadingIcon );
	this._cocktail.find('.section-flavors-laphroaig-cocktail-recipe').html( this.loadingIcon );
	this._cocktail.find('.section-flavors-laphroaig-cocktail-directions p').html( this.loadingIcon )
	// this.cocktailsGrid.html( this.loadingIcon );
	// remove the src from cocktail image
	this._cocktail.find('.cocktail-img').attr('src', '');
	// do the ajax request, but save 'this'
	// for use within our success function
	$.ajax({
		url:  pwpSimplicity.adminajax,
		type: 'POST',
		data: {
			action: 'load_flavor',
			flavor: _flavor
		},
		success: function(resp) {
			if (!resp.length) {
				return;
			}
			// loop through our cocktails
			for (var i = resp.length - 1; i >= 0; i--) {
				switch(resp[i].post_type) {
					case 'food':
						this.addFood( resp[i] );
					break;
					case 'cocktail':
						this.addCocktail( resp[i] );
					break;
					default: // flavor
						this.addFlavor( resp[i] );
					break;
				}
			}
			this.printCocktails();
		}.bind(this)
	});
};

pwpsLaphroaigFlavorWheel.prototype.addFood = function(food) {
	if ( food.post_title ) {
		this._food.find('p.ga-txt-md').html(food.post_title);
	}
	if ( food.post_content ) {
		this._food.find('.fooddescription').html(food.post_content);
	}
	if ( food.thumbnail[0] ) {
		this._food.css('background-image', 'url('+food.thumbnail[0]+')');
	}
	else {
		this._food.css('background-image', 'url("")');
	}
};

pwpsLaphroaigFlavorWheel.prototype.addFlavor = function(flavor) {
	if ( flavor.post_title ) {
		this._info.find('h3').html(flavor.post_title);
	}
	if ( flavor.post_content ) {
		this._info.find('.flavor-content').html(flavor.post_content);
	}
};

pwpsLaphroaigFlavorWheel.prototype.addCocktail = function(cocktail) {
	cocktail = cocktail || {};
	// save the first cocktail
	if (!this.firstCocktail) {
		this.firstCocktail = cocktail;
	}
	this.allCocktails.push(cocktail);
};

pwpsLaphroaigFlavorWheel.prototype.printCocktails = function() {
	// first cocktail
	if ( this.firstCocktail ) {
		if ( this.firstCocktail.post_title ) {
			this._cocktail.find('h2').html(this.firstCocktail.post_title);
		}
		if ( this.firstCocktail.post_content ) {
			this._cocktail.find('.section-flavors-laphroaig-cocktail-recipe').html(this.firstCocktail.post_content);
		}
		if ( this.firstCocktail.thumbnail[0] ) {
			this._cocktail.find('.cocktail-img').attr('src', this.firstCocktail.thumbnail[0]);
		}
		if ( this.firstCocktail.directions ) {
			this._cocktail.find('.section-flavors-laphroaig-cocktail-directions p').html(this.firstCocktail.directions);
		}
	}
	// set the column width for the cocktails grid
	// var _col = 'col3';
	// commenting the following code because the client now
	// wants to have all the cocltails be the same size.
	// -MV
	// if ( 1 === this.allCocktails.length ) {
	// 	_col = 'span12';
	// }
	// else if ( 2 === this.allCocktails.length ) {
	// 	_col = 'col2';
	// }
	// else {
	// 	var __columns = [2,3,4,5,6];
	// 	for (var i = __columns.length - 1; i >= 0; i--) {
	// 		if ( __columns[i] !== this.allCocktails.length
	// 			&& ( 0 === this.allCocktails.length % __columns[i] ) ) {
	// 			_col = 'col'+__columns[i];
	// 		}
	// 	}
	// }
	// empty the grid, then insert the cocktails as columns
	// this.cocktailsGrid.html('');
	// for (var i = this.allCocktails.length - 1; i >= 0; i--) {
	// 	this.cocktailsGrid.append('<div class="'+_col+'" style="background-image: url('+this.allCocktails[i].thumbnail[0]+');" data-index="'+i+'"></div>');
	// }

};

pwpsLaphroaigFlavorWheel.prototype.bindCocktailLightbox = function() {
	var _coktails = $('.load_coktail_ajax');
	var _this = this;
	_coktails.click(function(e){
		e.preventDefault();
		_this.lightbox.find('h3').html(_this.loadingIcon);
		_this.lightbox.find('.drink-image').html(_this.loadingIcon);
		_this.lightbox.find('.recipe').html(_this.loadingIcon);
		_this.lightbox.fadeIn('slow');
		var _postID = $(this).attr('data-post-id');
		if ( _postID && _postID.length ) {
			$.ajax({
				url: pwpSimplicity.adminajax,
				type: 'POST',
				data: {
					action: 'load_coktail_ajax',
					cocktail: _postID,
				},
				success: function(r) {

					if ( r.post_title ) {
						_this.lightbox.find('h3').html(r.post_title);
					}
					if ( r.thumbnail ) {
						_this.lightbox.find('.drink-image').html('<img src="'+r.thumbnail+'">');
					}
					if ( r.post_content ) {
						_this.lightbox.find('.recipe').html(r.post_content);
					}


					_this.lightbox.find('.section-more-cocktails-lightbox-close').one('click', function(){
						_this.lightbox.fadeOut('fast');
						_this.lightbox.find('h3').html('');
						_this.lightbox.find('.drink-image').html('');
						_this.lightbox.find('.recipe').html('');
					});
				}
			})
		}
	});
	return false;
	this.cocktailsGrid.find('> div').click(function(e){
		e.preventDefault();
		var $this = $(this),
		img       = $this.css('background-image')
		index     = $this.attr('data-index');

		return false;
	})
};