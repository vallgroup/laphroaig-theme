<?php
/**
 * Laphroaig functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Laphroaig
 */

// set the theme image sizes
add_image_size( 'pwps_laphroaig', 1200, 800, false );

/**
 * Load Laphroaig scrits and stylesheets
 */
function pwps_laphroaig_scripts() {
	// enqueue our stylesheet
	wp_register_style( 'lahroaig-fajall-one-gf', 'https://fonts.googleapis.com/css?family=Fjalla+One' );
	wp_register_style( 'lahroaig-georgia-gf'   , 'https://fonts.googleapis.com/css?family=Cantata+One' );

	wp_enqueue_style( 'laphroaig-style'  , get_stylesheet_uri(), array('lahroaig-fajall-one-gf','lahroaig-georgia-gf') );
	wp_enqueue_script( 'laphroaig-script', get_stylesheet_directory_uri() . '/js/pwp-simplicity.min.js', array('jquery') );
}
// load after our parent theme
add_action( 'wp_enqueue_scripts', 'pwps_laphroaig_scripts', 20 );

/**
 * output the flavor wheel vector
 */
function pwps_laphroaig_flavor_wheel() {
	echo file_get_contents( get_stylesheet_directory() . '/img/flavor-wheel/flavor_wheel.svg' );
}

/**
 * Load flavors via ajax
 *
 * @return string json object
 */
function pwps_laphroaig_load_flavor() {
	$action = ( isset( $_POST['action'] ) ) ? sanitize_text_field( $_POST['action'] ) : false;
	$flavor = ( isset( $_POST['flavor'] ) ) ? sanitize_text_field( $_POST['flavor'] ) : false;

	if ( $action && 'load_flavor' === $action ) {
		$pairings = new WP_Query( array(
			'post_type' => array( 'food', 'flavor', 'cocktail' ),
			'tax_query' => array(
				array(
					'taxonomy' => 'flavor_tag',
					'field'    => 'slug',
					'terms'    => $flavor,
				),
			),
		) );
		$response = array();
		foreach ($pairings->get_posts() as $post) {
			$array_1 = array(
				'post_type'    => $post->post_type,
				'post_title'   => $post->post_title,
				'post_content' => '<p class="ga-txt-lg">'.nl2br($post->post_content).'</p>',
			);
			$array_2 = array(
				'thumbnail' => ('flavor' !== $post->post_type) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'pwps_laphroaig' ) : '',
			);
			if ( 'cocktail' == $post->post_type ) {
				$array_2['directions'] = premise_get_value( 'laphroaig_coktail[directions]', array( 'context' => 'post', 'id' => $post->ID ) );
			}
			$response[] = array_merge( $array_1, $array_2 );
		}
		echo wp_send_json( $response );
	}

	die();
}
add_action( 'wp_ajax_load_flavor', 'pwps_laphroaig_load_flavor' );
add_action( 'wp_ajax_nopriv_load_flavor', 'pwps_laphroaig_load_flavor' );

/**
 * Load cocktails vi ajax
 *
 * @return string json object
 */
function pwps_laphroaig_load_cocktail() {
	$action = ( isset( $_POST['action'] ) ) ? sanitize_text_field( $_POST['action'] ) : false;
	$cocktail = ( isset( $_POST['cocktail'] ) ) ? sanitize_text_field( $_POST['cocktail'] ) : false;
// var_dump($cocktail);
	if ( $action && 'load_coktail_ajax' === $action ) {
		$post = get_post( $cocktail );
		$response = array(
			'post_type'    => $post->post_type,
			'post_title'   => $post->post_title,
			'post_content' => '<p class="ga-txt-lg">'.nl2br($post->post_content).'</p>',
			'directions'   => premise_get_value( 'laphroaig_coktail[directions]', array( 'context' => 'post', 'id' => $post->ID ) ),
			'thumbnail'    => wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ),
		);
		echo wp_send_json( $response );
	}

	die();
}
add_action( 'wp_ajax_load_coktail_ajax', 'pwps_laphroaig_load_cocktail' );
add_action( 'wp_ajax_nopriv_load_coktail_ajax', 'pwps_laphroaig_load_cocktail' );


if ( class_exists( 'Premise_WP' ) ) {
	// register our custom post types
	if ( class_exists( 'PremiseCPT' ) ) {
		$cpt_args  = array( 'supports' => array( 'thumbnail', 'editor', 'title' ) );
		// register the flavors custom post type
		$flavors   = new PremiseCPT( 'flavor' );
		// register the foods custom post type
		$foods     = new PremiseCPT( 'food', $cpt_args );
		// register the cocktails custom post type
		$cocktails = new PremiseCPT( 'cocktail', $cpt_args );
		// add tags support for each cpt
		$tax_args  = array( 'hierarchical' => false );
		$flavors->register_taxonomy( 'flavor_tag', $tax_args );
		$foods->register_taxonomy( 'flavor_tag', $tax_args );
		$cocktails->register_taxonomy( 'flavor_tag', $tax_args );
	}
	// register our metaboxes
	if ( function_exists( 'pwp_add_metabox' ) ) {
		pwp_add_metabox( 'Cocktail Info', 'cocktail', array(
			'name_prefix' => 'laphroaig_coktail',
			array(
				'type'        => 'textarea',
				'name'        => '[directions]',
				'label'       => 'Directions',
				'placeholder' => 'Stir. Serve on the rocks.',
				'context'     => 'post',
			),
		), 'laphroaig_coktail' );
	}
}