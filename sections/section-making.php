<?php
/**
 * @package Laphroaig\sections
 */
?>
<section id="Making-Laphroaig" class="section-making-laphroaig" style="background-image: url(<?php echo esc_url( get_stylesheet_directory_uri().'/img/harnessigng-power-bg.png' ); ?>);">
	<div class="inner_section">
		<div class="section-making-laphroaig-header pwp-align-center">
			<h2>the making of laphroaig<sup>®</sup></h2>
			<div class="rugged-bar rugged-bar-white"></div>

			<br>
			<p>For over 200 years, Laphroaig<sup>®</sup> has distilled their notably divisive whisky on the southern coast of Islay.<br>Let’s open a bottle and see exactly what’s inside.</p>
		</div>

		<div class="section-making-laphroaig-ingredients" >
			<div class="section-making-laphroaig-ingredients-water section-making-laphroaig-ingredients-i span8 pwp-float-right">
				<div class="pwp-inline">
					<div class="section-making-laphroaig-ingredients-icon col2 pwp-vertical-align-middle pwp-align-center">
						<?php echo file_get_contents( get_stylesheet_directory_uri().'/img/making-laphroaig/water.svg' ); ?>
					</div>
					<div class="section-making-laphroaig-ingredients-text col2 pwp-vertical-align-middle">
						<h3>Water</h3>
						<p class="medium">Soft, peated water sourced exclusively from Loch Kilbride. About 15% of Laphroaig’s<sup>&reg;</sup> flavor comes from this water.</p>
					</div>
				</div>
			</div>
			<div class="section-making-laphroaig-ingredients-peat section-making-laphroaig-ingredients-i span8 pwp-float-left pwp-align-right">
				<div class="pwp-inline">
					<div class="section-making-laphroaig-ingredients-text col2 pwp-vertical-align-middle">
						<h3>Peat</h3>
						<p class="medium">A mix of heather, lichen and a higher moss ratio. Laphroaig<sup>&reg;</sup> is one of few Scottish distilleries to hand-cut its own peat to retain more moisture, creating a slower burn for a more intense peat smoke on the malt.</p>
					</div>
					<div class="section-making-laphroaig-ingredients-icon col2 pwp-vertical-align-middle pwp-align-center">
						<?php echo file_get_contents( get_stylesheet_directory_uri().'/img/making-laphroaig/peat.svg' ); ?>
					</div>
				</div>
			</div>
			<div class="section-making-laphroaig-ingredients-barley section-making-laphroaig-ingredients-i span8 pwp-float-right">
				<div class="pwp-inline">
					<div class="section-making-laphroaig-ingredients-icon col2 pwp-vertical-align-middle pwp-align-center">
						<?php echo file_get_contents( get_stylesheet_directory_uri().'/img/making-laphroaig/barley.svg' ); ?>
					</div>
					<div class="section-making-laphroaig-ingredients-text col2 pwp-vertical-align-middle">
						<h3>malted Barley</h3>
						<p class="medium">One of the few remaining Scottish distilleries to malt its own barley, maltings are kiln dried at industry low temperatures, resulting in a higher phenol spec of 40 to 60 ppm.</p>
					</div>
				</div>
			</div>
			<div class="section-making-laphroaig-ingredients-still section-making-laphroaig-ingredients-i span8 pwp-float-left pwp-align-right">
				<div class="pwp-inline">
					<div class="section-making-laphroaig-ingredients-text col2 pwp-vertical-align-middle">
						<h3>Distillation</h3>
						<p class="medium">Laphroaig<sup>&reg;</sup> runs the longest head in the industry, with the spirit run’s first cut made at 45 minutes, making it less sweet while preserving its more tarry, medicinal, peaty notes.</p>
					</div>
					<div class="section-making-laphroaig-ingredients-icon col2 pwp-vertical-align-middle pwp-align-center">
						<?php echo file_get_contents( get_stylesheet_directory_uri().'/img/making-laphroaig/still.svg' ); ?>
					</div>
				</div>
			</div>
			<div class="section-making-laphroaig-ingredients-islay section-making-laphroaig-ingredients-i span8 pwp-float-right">
				<div class="pwp-inline">
					<div class="section-making-laphroaig-ingredients-icon col2 pwp-vertical-align-middle pwp-align-center">
						<?php echo file_get_contents( get_stylesheet_directory_uri().'/img/making-laphroaig/islay.svg' ); ?>
					</div>
					<div class="section-making-laphroaig-ingredients-text col2 pwp-vertical-align-middle">
						<h3>Islay</h3>
						<p class="medium">The salty air off the Atlantic. The moss-laden peat bogs. Islay is as much responsible for the richness and character of Laphroaig<sup>&reg;</sup> as the people who live here.</p>
					</div>
				</div>
			</div>
			<div class="section-making-laphroaig-ingredients-bottle">
				<img src="<?php echo esc_url( get_stylesheet_directory_uri().'/img/Bottle.png' ); ?>">
			</div>
		</div>
	</div>
</section>