<?php
/**
 * @package Laphroaig\sections
 */
?>
<section id="Harnessing-Power-Peat" class="section-harnessing-power" style="background-image: url(<?php echo esc_url( get_stylesheet_directory_uri().'/img/harnessigng-power-bg.png' ); ?>);">

	<div class="inner_section">
		<div class="section-harnessing-power-header">
			<h2 class="color-white">harnessing the power of peat</h2>
			<div class="rugged-bar rugged-bar-white"></div>
		</div>

		<p>Rich, complex and notoriously smoky, Laphroaig<sup>&reg;</sup> is a single malt not to be taken lightly. But for the brave who undertake the venture, there are endless peaty rewards for the taking.</p>
		<p>When seeking smoke and bolder flavours, Laphroaig<sup>&reg;</sup> 10 Year Old and Select offer two unique ways way to impart these traits. Come explore their diverse palates and start crafting cocktails as unique as the Scotch itself.</p>
	</div>

</section>