<?php
/**
 * @package Laphroaig\sections
 */
?>
<section id="Flavours-Laphroaig" class="section-flavors-laphroaig full-width">
	<div class="inner_section">
		<div class="section-flavors-laphroaig-header pwp-align-center">
			<h2>the flavours of laphroaig<sup>®</sup></h2>
			<div class="rugged-bar rugged-bar-green"></div>

			<br>
			<p>Creating cocktails with Laphroaig<sup>&reg;</sup> is complex business. But master the art and you’ll have a world of flavours at hand. The tool below matches the notes you might find in our Scotch with complimentary and balancing flavors. Pour yourself a dram, select a flavor that stands out and start experimenting. Sláinte!</p>
		</div>

		<div class="section-flavors-laphroaig-wheel-cont">
			<div class="section-flavors-laphroaig-wheel-scroller">
				<div class="pwp-vertical-align-middle section-flavors-laphroaig-wheel">
					<div class="section-flavors-laphroaig-wheel-shadow">
						<?php pwps_laphroaig_flavor_wheel(); ?>
					</div>
				</div>
				<div class="pwp-vertical-align-middle section-flavors-laphroaig-wheel-info">
					<h3></h3>
					<h4>PAIRS WELL WITH</h4>
					<div class="flavor-content"></div>
					<div class="section-flavors-laphroaig-wheel-info-border"></div>
				</div>
			</div>
		</div>

		<div class="section-flavors-laphroaig-arrow">
			<p>See some of our favorite pairings below based on your flavor selection.</p>
			<i class="fa fa-angle-down fa-3x"></i>
		</div>
	</div>
</section>