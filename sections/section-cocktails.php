<?php
/**
 * @package Laphroaig\sections
 */

$_cocktails = new WP_Query(array(
	'post_type' => 'cocktail',
	'posts_per_page' => -1,
));

if ($_cocktails->have_posts()) : ?>
<section class="section-more-cocktails">
	<div class="inner_section">
		<div class="section-more-cocktails-header">
			<h3>experience more</h3>
			<h2>laphroaig<sup>®</sup> cocktails</h2>
			<div class="rugged-bar rugged-bar-white"></div>
		</div>

		<div class="section-more-cocktails-grid pwp-block pwp-align-center" style="font-size: 0;">
			<?php while($_cocktails->have_posts()) : $_cocktails->the_post();
				$img = wp_get_attachment_url( get_post_thumbnail_id() ); ?>
				<div class="col3 pwp-inline-block load_coktail_ajax" style="background-image: url(<?php echo esc_url( $img ); ?>);" data-post-id="<?php echo esc_attr( $post->ID ); ?>"></div>
			<?php endwhile; ?>
		</div>
	</div>

	<div class="section-more-cocktails-lightbox" style="display: none;">
		<div class="section-more-cocktails-lightbox-inner inner_section">
			<div class="section-more-cocktails-lightbox-close"></div>
			<div class="section-more-cocktails-lightbox-header">
				<h3>experience more</h3>
				<div class="rugged-bar rugged-bar-white"></div>
			</div>
			<div class="section-more-cocktails-lightbox-content pwp-row not-responsive">
				<div class="col2 drink-image"></div>
				<div class="col2 recipe"></div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>