<?php
/**
 * @package Laphroaig\sections
 */
?>
<section id="Inspirational-Laphroaig-Cocktails" class="section-inspirational-cocktails full-width">

		<div class="section-flavors-laphroaig-cocktail">
			<div class="inner_section">
				<div class="section-flavors-laphroaig-cocktail-header">
					<h3>what we crafted</h3>
					<h2>kiss of islay</h2>
					<div class="rugged-bar rugged-bar-white"></div>
				</div>
				<div class="section-flavors-laphroaig-cocktail-content">
					<div class="pwp-inline">
						<div class="col2 pwp-vertical-align-middle">
							<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/img/kiss-of-islay.png' ); ?>" class="cocktail-img">
						</div>
						<div class="section-flavors-laphroaig-cocktail-recipe col2 pwp-vertical-align-middle">
							<p class="small"><b>1.5oz</b> Makers Mark<sup>&reg;</sup></p>
							<p class="small"><b>0.5oz</b> Laphroaig<sup>&reg;</sup> 10</p>
							<p class="small"><b>0.25oz</b> Simple syrup</p>
							<p class="small"><b>Dash</b> Angostura bitters</p>
							<p class="small"><b>Dash</b> Chocolate bitters</p>
						</div>
					</div>
				</div>
			</div>
			<div class="section-flavors-laphroaig-cocktail-directions">
				<p class="ga-txt-md">Stir. Serve on the rocks.</p>
			</div>
		</div>

		<div class="section-flavors-laphroaig-food">
			<div class="section-flavors-laphroaig-food-header">
				<h2>Fare To Match</h2>
				<div class="rugged-bar rugged-bar-white"></div>
			</div>
			<div class="inner_section"></div>
				<div class="section-flavors-laphroaig-food-name">
					<div class="section-flavors-laphroaig-cocktail-directions">
						<p class="ga-txt-md">Oysters</p>
						<!-- <h3>Oysters</h3> -->
					</div>
				</div>
		</div>

</section>