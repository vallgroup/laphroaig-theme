<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Laphroaig
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info inner_section">

			<div class="social pwp-inline">
				<a href="http://instagram.com/laphroaig" class="social-icon social-instagram pwp-inline-block"></a>
				<a href="http://facebook.com/laphroaig" class="social-icon social-facebook pwp-inline-block"></a>
				<a href="http://youtube.com/laphroaig" class="social-icon social-youtube pwp-inline-block"></a>
				<a href="http://twitter.com/laphroaig" class="social-icon social-twitter pwp-inline-block"></a>
			</div>

			<div class="terms-policy pwp-align-center">
				<div class="pwp-row not-responsive">
					<div class="col2">
						<a href="#Terms of Use">Terms of Use</a>
					</div>
					<div class="col2">
						<a href="#Privacy Policy">Privacy Policy</a>
					</div>
				</div>
				<p>Enjoy responsibly www.drinksmart.com Read the Laphroaig Responsibility Statement here - Laphroaig adheres to the SWA Code of Practice for the Responsible Marketing and Promotion of Scotch Whisky. D. Johnston & Company (Laphroaig) Ltd, Springburn Bond, Carlisle St, Glasgow G21 1EQ, registered in the United Kingdom, registration number SC028072. Laphroaig<sup>&reg;</sup> Single Malt Whisky, 43% alc./vol.</p>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
