<?php
/**
 * Laphroaig Single Page Micro Site.
 *
 * This is the main template for the site.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Laphroaig
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="front-page site-main">

		<?php get_template_part( 'sections/section', 'harness'); ?>

		<?php get_template_part( 'sections/section', 'explore'); ?>

		<?php get_template_part( 'sections/section', 'making'); ?>

		<?php get_template_part( 'sections/section', 'flavors'); ?>

		<?php get_template_part( 'sections/section', 'inspirational'); ?>

		<?php get_template_part( 'sections/section', 'cocktails'); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
