<?php
/**
 * @package Laphroaig\sections
 */
?>
<section id="Laphroaig-select" class="section-select" style="background-image: url(<?php echo esc_url( get_stylesheet_directory_uri().'/img/Select_BG.png' ); ?>);">
	<div class="inner_section">
		<div class="section-explore-expressions-header">
			<h2 class="color-white">explore the expressions</h2>
			<div class="rugged-bar rugged-bar-green"></div>

			<p>Creating the perfect Laphroaig<sup>&reg;</sup> cocktail begins with choosing the right Laphroaig<sup>&reg;</sup> for the job.</p>
		</div>

		<div class="pwp-inline not-responsive flush-columns section-select-bottle">
			<div class="col2 pwp-vertical-align-middle section-select-bottle-text">
				<h3><span>Laphroaig<sup>®</sup></span><br>Select</h3>
				<p>Laphroaig Select is an excellent base for creating smoky cocktails, adding the perfect amount of peat while balancing well with other flavors.</p>
				<p>Matured in five different barrel types, the signature Laphroaig smokiness is complimented by a gentle sweetness, creating a whisky that is artfully peated, approachable and perfectly balanced.</p>
			</div>
			<div class="col2 pwp-vertical-align-middle">
				<img src="<?php echo esc_url( get_stylesheet_directory_uri().'/img/Select.png' ); ?>">
			</div>
		</div>

		<!-- <div class="pwp-align-center">
			<p>Create smoky cocktails using Laphroaig<sup>&reg;</sup> Select as a base to add peat without overpowering other flavors.</p>
		</div> -->

		<div class="section-select-tasting-notes">
			<div class="section-explore-expressions-tasting-notes-header">
				<h3>tasting notes</h3>
				<div class="rugged-bar rugged-bar-green"></div>

				<div class="pwp-inline">
					<div class="col2 pwp-vertical-align-top">
						<h5>NOSE</h5>
						<p>Peat first, then ripe red fruits with a hint of dryness</p>
					</div>

					<div class="col2 pwp-vertical-align-top">
						<h5>PALATE</h5>
						<p>Sweet up front then classic dry, peaty, ashy flavours followed by a rich finish</p>
					</div>
				</div>

				<div class="pwp-inline">
					<div class="col2 pwp-vertical-align-top">
						<h5>BODY</h5>
						<p>Full</p>
					</div>

					<div class="col2 pwp-vertical-align-top">
						<h5>FINISH</h5>
						<p>Long lingering and floral, with marzipan and limes at the end</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="Explore-Expressions" class="section-explore-expressions" style="background-image: url(<?php echo esc_url( get_stylesheet_directory_uri().'/img/barrel_bg.png' ); ?>);">
	<div class="inner_section">

		<div class="pwp-inline not-responsive flush-columns section-explore-expressions-bottle">
			<div class="col2 pwp-vertical-align-middle">
				<img src="<?php echo esc_url( get_stylesheet_directory_uri().'/img/Bottle.png' ); ?>">
			</div>
			<div class="col2 pwp-vertical-align-middle section-explore-expressions-bottle-text">
				<h3>Laphroaig<sup>&reg;</sup><br>10 year old</h3>
				<p>Perfect for infusing a subtle smoke via float, rinse or atomizer, wielding the powerful peatiness of 10 Year Old is a delicate art worth mastering.</p>
				<p>The benchmark Islay malt, 10 Year Old has a nose and taste that deliver a unique measure of Islay peat smoke, tangy, salt-laden air and an echo of sweetness at the end.</p>
			</div>
		</div>

		<!-- <div class="pwp-align-center">
			<p>The deep peatiness will stand out in bold, peat-forward cocktails or impart a subtle smoke via float, rinse or atomizer.</p>
		</div> -->

		<div class="section-explore-expressions-tasting-notes">
			<div class="section-explore-expressions-tasting-notes-header">
				<h3>tasting notes</h3>
				<div class="rugged-bar rugged-bar-green"></div>

				<div class="pwp-inline">
					<div class="col2 pwp-vertical-align-top">
						<h5>NOSE</h5>
						<p>Huge smoke, seaweedy and “medicinal” with a hint of sweetness</p>
					</div>

					<div class="col2 pwp-vertical-align-top">
						<h5>PALATE</h5>
						<p>Surprising sweetness with a hint of saltand layers of peatiness</p>
					</div>
				</div>

				<div class="pwp-inline">
					<div class="col2 pwp-vertical-align-top">
						<h5>BODY</h5>
						<p>Full</p>
					</div>

					<div class="col2 pwp-vertical-align-top">

					<h5>FINISH</h5>
					<p>Long and lingering</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
